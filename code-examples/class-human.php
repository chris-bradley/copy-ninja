<?php

class Human
{
    /**
     * @var string
     */
    protected $gender;
    /**
     * @var string
     */
    protected $hairColor;
    /**
     * @var array
     */
    protected $clothes;

    /**
     * Human constructor.
     * @param $gender
     * @param $hairColor
     * @param array $clothes
     */
    public function __construct($gender, $hairColor, $clothes = array())
    {
        $this->gender = $gender;
        $this->hairColor = $hairColor;
        $this->clothes = $clothes;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @return string
     */
    public function getHairColor(): string
    {
        return $this->hairColor;
    }

    /**
     * @return array
     */
    public function getClothes(): array
    {
        return $this->clothes;
    }
}

/**
 * Run this script to see the result
 */
$human = new Human('male', 'brown', array('purple shirt', 'pants'));

echo $human->getGender() . ', ';
echo $human->getHairColor() . ', ';

foreach ($human->getClothes() as $item) {
    echo $item;
}
