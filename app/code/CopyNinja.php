<?php

class CopyNinja {
    const VIDEO_TYPE_MP4 = 'MP4';
    const IMAGE_TYPE_JPG = 'JPG';
    const IMAGE_TYPE_RAW = 'CR3';

    /**
     * Source directory (location of files to copy)
     * @var string
     */
    protected $sourceDir;
    /**
     * Export directory (where files are copied to)
     * @var string
     */
    protected $exportDir;
    /**
     * File types to display on website
     *
     * @var array
     */
    protected $extDisplayWhitelist = array(
        self::IMAGE_TYPE_RAW,
        self::VIDEO_TYPE_MP4
    );

    /**
     * CopyNinja constructor
     *
     * @param $sourceDir string
     * @param $exportDir string
     * @param $selection array
     */
    public function __construct($sourceDir, $exportDir, $selection = array())
    {
        $this->sourceDir = $sourceDir;
        $this->exportDir = $exportDir;

        if (!empty($selection)) {
            $this->copyFiles($selection);
        }
    }

    /**
     * Load a list of files from source dir
     *
     * @return array
     */
    public function getFiles(): array
    {
        $files = scandir($this->sourceDir);
        $output = array();

        foreach ($files as $file) {
            $name = explode('_', $file);
            foreach ($name as $piece) {
                if(1 === preg_match('~[0-9]~', $piece)){
                    $id = array_shift(explode('.', $piece));
                    $ext = array_pop(explode('.', $piece));
                    if (in_array($ext, $this->extDisplayWhitelist)) {
                        if (!isset($output[$ext])) {
                            $output[$ext] = array();
                        }
                        $output[$ext][] = $id;
                    }
                    break;
                }
            }
        }

        return $output;
    }

    /**
     * Copy the selection of files from source dir to export dir
     *
     * @param $selection array
     */
    private function copyFiles($selection)
    {
        $files = scandir($this->sourceDir);
        $filesToCopy = array();
        $extWhitelist = array(
            self::IMAGE_TYPE_RAW,
            self::IMAGE_TYPE_JPG,
            self::VIDEO_TYPE_MP4
        );

        foreach ($files as $file) {
            $name = explode('_', $file);
            foreach ($name as $piece) {
                if(1 === preg_match('~[0-9]~', $piece)){
                    $id = array_shift(explode('.', $piece));
                    $ext = array_pop(explode('.', $piece));
                    if (in_array($ext, $extWhitelist) &&
                        in_array($id, $selection) &&
                        !in_array($id, $filesToCopy)) {
                        $filesToCopy[] = $file;
                    }
                    break;
                }
            }
        }

        if (!empty($filesToCopy)) {
            $output = array();
            $counts = array(
                'total' => 0,
                'error' => 0,
            );

            foreach ($filesToCopy as $filename) {
                $ext = array_pop(explode('.', $filename));
                try {
                    copy($this->sourceDir . $filename, $this->exportDir . $ext . '/' . $filename);
                    isset($counts[$ext]) ? $counts[$ext]++ : $counts[$ext] = 1;
                    $counts['total']++;
                } catch (Exception $e) {
                    $counts['error']++;
                    $output[] = sprintf('Error #%d copying: "%s"', $counts['error'], $filename);
                }
            }

            $output[] = 'Total Errors: ' . $counts['error'];
            $output[] = 'Copied: ';

            foreach ($counts as $ext => $count) {
                if ($ext != 'error' && $ext != 'total') {
                    $output[count($output)-1] .= $count . ' ' . $ext . ';';
                }
            }

            $output[] = 'Total Copied: ' . $counts['total'];
            $output[] = 'Export Directory: ' . $this->exportDir;

            foreach ($output as $line) {
                echo $line . ' ';
            }
        }
    }
}