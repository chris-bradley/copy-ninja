How to code for beginners | PHP (2020) Episode 1

The script we discussed in this video is located within the app/code directory. You may use the script.php to interact with this class.

If you would like a simple exercise, review "code-examples/class-human.php". This script can be ran independently.

Also, we didn't discuss the index.phtml. This is a small web page that loads image IDs from the source directory, so that I can select them and build out a string to use in script.php. You can set this directory up as your root web folder and then hit the index.php, which will load the index.phtml.